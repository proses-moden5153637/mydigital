import { sleep } from 'k6';
import http from 'k6/http';

export const options = {
  duration: '20s',
  vus: 50,
  thresholds: {
    http_req_duration: ['p(45)<1000'], // 45 percent of response times must be below 1000ms
  },
};

export default function () {
  http.get('https://mydigital.cloud-connect.asia/');
  sleep(3);
}
